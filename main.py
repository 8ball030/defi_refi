"""
This script is the entry point into the crude v1 version of the

recurring payment notification service


"""
import csv
from datetime import datetime, timedelta
import time
from mobile_notifier import Notifier
from copy import deepcopy
from dateutil.relativedelta import relativedelta


class Payment:
    notice_period = 1 # day

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def is_due(self):
        """Check if the payment is due"""
        parsed_date = datetime.strptime(self.due_date, "%Y-%m-%d" )
        if (parsed_date - datetime.now()) < timedelta(days=self.notice_period):
            # we set our new due date to be 1 month in the future
            print(f"Payment Due! {self}")
            new_date = parsed_date + relativedelta(months=1)
            self.due_date = new_date.strftime("%Y-%m-%d")
            return True

    def __repr__(self):
        return f"Payment {self.payment_description} for : {self.amount} due on day {self.due_date}"

    def __str__(self):
        return self.__repr__()


def main():
    notifier = Notifier()
    while True:
        records = []
        with open('payment_store.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                payment = Payment(**row)
                if payment.is_due():
                    notifier.send(str(payment))
                    row["due_date"] = payment.due_date
                records.append(row)
        with open('payment_store.csv', 'w', newline='') as csvfile:
            fieldnames = ["payment_description", "amount", "due_date"]
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            [writer.writerow(row) for row in records]
        print(f"Waiting till tomorrow...{datetime.now()}")
        time.sleep(60 * 60 * 24)


if __name__ == "__main__":
    main()
