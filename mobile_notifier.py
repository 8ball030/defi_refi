"""
notifier class to allow the agents to notify the users

documentation available here;

https://pypi.org/project/notify-run/
"""
# import os
from subprocess import call, check_output
import re
import requests
import time
# import shutil


class Notifier:
    """
    Interactors with the notifactor server
    """
    command = ["notify-run", "register", ]
    def __init__(self):
        call(["rm",  "/home/tom/.config/notify-run"])
        self.raw = check_output(self.command)
        self.grep_url()

    def grep_url(self):
        pattern = "https:\/\/(.*)\\\\nT"
        try:
            self.url = re.findall(pattern, str(self.raw))[0]
        except:
            print("Failed to register to the notification channel")
            time.sleep(1)
            self.grep_url()
        print("Registered to Notification Channel.\nJoin url: https://"+self.url)

    def send(self, message):
        requests.post("https://" + self.url, data=message)

if __name__ == "__main__":
    n = notifier()
    n.send("Test")
